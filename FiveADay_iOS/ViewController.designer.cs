// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace FiveADay_iOS
{
	[Register ("ViewController")]
	partial class ViewController
	{
		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel bigCount { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		FiveADay_iOS.ButtonAdd fruitButton { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIButton historyButton { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		FiveADay_iOS.ButtonAdd vegeButton { get; set; }

		void ReleaseDesignerOutlets ()
		{
			if (bigCount != null) {
				bigCount.Dispose ();
				bigCount = null;
			}
			if (fruitButton != null) {
				fruitButton.Dispose ();
				fruitButton = null;
			}
			if (historyButton != null) {
				historyButton.Dispose ();
				historyButton = null;
			}
			if (vegeButton != null) {
				vegeButton.Dispose ();
				vegeButton = null;
			}
		}
	}
}
