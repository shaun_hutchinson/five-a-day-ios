﻿using System;
using Foundation;
using UIKit;
using System.Collections.Generic;

namespace FiveADay_iOS
{
    public partial class ViewController : UIViewController
    {
        private String SavedDate;

        public ViewController(IntPtr handle) : base(handle)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            bigCount.MinimumFontSize = 150;
            bigCount.AdjustsFontSizeToFitWidth = false;
            SavedDate = Defaults.GetDate();

            Console.WriteLine(Count.GetCount() + " the count");
            //new day
            String TodayDate = DateTime.Today.ToString();
            if(!TodayDate.Equals(SavedDate))
            {
                Count.Reset();
                //obtain how many eaten previous day before clearing
                int lastCount = FoodCollection.FoodList.Count;
                FoodCollection.FoodList.Clear();
                //reset buttons to be used again
                fruitButton.UserInteractionEnabled = true;
                vegeButton.UserInteractionEnabled = true;
                fruitButton.Alpha = 1.0f;
                vegeButton.Alpha = 1.0f;
                if(lastCount == 5)
                {
                    showAlert("Info", "You ate your 5 a day! Good work!");
                }
                else
                {
                    showAlert("Info", String.Format("You ate {0} fruit and vegetables yesterday, try again today!", lastCount));
                }
                Console.WriteLine(Count.GetCount() + ": the count about to be written to string");
                bigCount.Text = Count.GetCount().ToString();
            }
            else
            {
                bigCount.Text = Count.GetCount().ToString();
            }

            historyButton.TouchUpInside += HistoryButton_TouchUpInside;
            

        }

        private void HistoryButton_TouchUpInside(object sender, EventArgs e)
        {
            if (FoodCollection.FoodList.Count == 0)
            {
                showAlert("Wait!", "Eat something first!");
            }
            else
            {
                HistoryTableController history = this.Storyboard.InstantiateViewController("historyTable") as HistoryTableController;
                this.NavigationController.PushViewController(history, true);
            }
        }

        public override void ViewDidAppear(bool animated)
        {
            base.ViewDidAppear(animated);
            bigCount.Text = Count.GetCount().ToString();
            Console.WriteLine("hey im here now");
            
        }

        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);
            if (Count.GetCount() == 0)
            {
                showAlert("Congratulations!", "You ate your 5 a day!");
                fruitButton.UserInteractionEnabled = false;
                vegeButton.UserInteractionEnabled = false;
                fruitButton.Alpha = 0.5f;
                vegeButton.Alpha = 0.5f;

            }
        }

        public override void DidReceiveMemoryWarning()
        {
            base.DidReceiveMemoryWarning();
            System.Diagnostics.Debug.WriteLine("oh shit this isn't good :(");
            // Release any cached data, images, etc that aren't in use.
        } 

        public void showAlert(String Title, String Message)
        {
            //create alert
            var completeAlertController = UIAlertController.Create(Title, Message, UIAlertControllerStyle.Alert);

            //add action
            completeAlertController.AddAction(UIAlertAction.Create("OK", UIAlertActionStyle.Default, null));

            //present alert
            PresentViewController(completeAlertController, true, null);
        }

        public override void PrepareForSegue(UIStoryboardSegue segue, NSObject sender)
        {
            base.PrepareForSegue(segue, sender);
        }
    }
}