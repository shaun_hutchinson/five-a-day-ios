using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace FiveADay_iOS
{
	partial class FruitViewController : UICollectionViewController
	{
        static NSString fruitCellId = new NSString("FruitCell");
        static NSString headerId = new NSString("Header");

        static String[] imageNames = { "apple.png", "apricot.png", "banana.png", "cucumber.png", "grapefruit.png", "grapes.png", "kiwifruit.png", "mango.png", "orange.png", "peach.png", "pear.png", "pineapple.png", "plum.png", "strawberry.png", "tomato.png" };
        private String Date = DateTime.Now.ToShortTimeString();

        public FruitViewController (IntPtr handle) : base (handle)
		{
		}

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            CollectionView.RegisterClassForCell(typeof(FruitCell), fruitCellId);

        }

        public override nint NumberOfSections(UICollectionView collectionView)
        {
            return 1;
        }

        public override nint GetItemsCount(UICollectionView collectionView, nint section)
        {
            return imageNames.Length;
        }

        //the item click to add fruit to eaten items
        public override void ItemHighlighted(UICollectionView collectionView, NSIndexPath indexPath)
        {
            var cell = collectionView.CellForItem(indexPath);
            String Name = (imageNames[indexPath.Row].Substring(0, imageNames[indexPath.Row].Length-4));
            EventItem item = new EventItem(Name, Date, true);
            FoodCollection.FoodList.Add(item);

            Count.Decrement();
            Console.WriteLine(Count.GetCount() + ", I should of decremented here");
            this.NavigationController.PopViewController(true);

        }

        public override UICollectionViewCell GetCell(UICollectionView collectionView, NSIndexPath indexPath)
        {
            var fruitCell = (FruitCell)collectionView.DequeueReusableCell(fruitCellId, indexPath);
            fruitCell.Image = UIImage.FromFile("Images/" + imageNames[indexPath.Row]);
            return fruitCell;
        }

    }
}
