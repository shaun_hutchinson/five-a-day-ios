using CoreGraphics;
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace FiveADay_iOS
{
    partial class FruitCell : UICollectionViewCell
    {

        UIImageView imageView;

        [Export("initWithFrame:")]
        public FruitCell(CGRect frame) : base(frame)
        {
            //creates the cell with placeholder image inside


            imageView = new UIImageView(UIImage.FromFile("Images/apple.png"));
            imageView.Center = ContentView.Center;
            imageView.Transform = CGAffineTransform.MakeScale(0.45f, 0.45f);
            imageView.UserInteractionEnabled = true;
            

            ContentView.AddSubview(imageView);
        }

        public UIImage Image
        {
            set
            {
                imageView.Image = value;
            }
        }
    }
}
