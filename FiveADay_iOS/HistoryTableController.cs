using Foundation;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using UIKit;

namespace FiveADay_iOS
{
	partial class HistoryTableController : UITableViewController
	{
        static NSString historyCellId = new NSString("HistoryCell");
        public List<EventItem> HistoryItems;

        public HistoryTableController (IntPtr handle) : base (handle)
		{
            TableView.RegisterClassForCellReuse(typeof(UITableViewCell), historyCellId);
            TableView.Source = new HistoryDataSource(this);
            HistoryItems = FoodCollection.FoodList;     
		}

        class HistoryDataSource : UITableViewSource
        {
            HistoryTableController controller;

            public HistoryDataSource(HistoryTableController controller)
            {
                this.controller = controller;
            }


            public override nint RowsInSection(UITableView tableview, nint section)
            {
                return FoodCollection.FoodList.Count;
            }

            public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
            {
                var cell = tableView.DequeueReusableCell(HistoryTableController.historyCellId);
                var row = indexPath.Row;

                String Value = "\t" + controller.HistoryItems[row].Name + "\t\t\t" + controller.HistoryItems[row].Date;
                cell.BackgroundColor = UIColor.FromRGB(102, 153, 0);
                //cell.TextLabel.TextAlignment = UITextAlignment.Center;
                cell.TextLabel.Text = Value;
                return cell;
            }
        }


	}
}
