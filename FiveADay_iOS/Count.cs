﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FiveADay_iOS
{
    public static class Count
    {
        static nint number;

        public static void Reset()
        {
            number = 5;
        }

        public static void Decrement()
        {
            if(GetCount() > 0)
            {
                number--;
            }
            
        }
        
        public static nint GetCount()
        {
            return number;
        }

        public static void SetCount(nint value)
        {
            number = value;
        }



    }
}
