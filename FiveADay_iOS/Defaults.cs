﻿using Foundation;
using System;
using System.Collections.Generic;
using System.Text;

namespace FiveADay_iOS
{
    public static class Defaults
    {
        static NSUserDefaults Details = NSUserDefaults.StandardUserDefaults;


        public static nint GetLastCount()
        {
            Boolean check = firstLaunch();
            if (!check)
            {
                Console.WriteLine("hello, its me");
                Details.SetBool(true, "firstTime");
                return 5;
            }
            else
            {
                nint temp = Details.IntForKey("count");
                Console.WriteLine(temp + ": the count");
                return temp;
            }
        }

        public static void SaveCount()
        {
            Details.SetInt(Count.GetCount(), "count");
            Console.WriteLine("Count saved");
        }

        public static List<EventItem> GetList()
        {
            String temp = Details.StringForKey("foodList");
            List<EventItem> EmptyList = new List<EventItem>();
            if (temp != null)
            {
                return XMLManager.DeserializeObject(temp);
            }
            else
            {
                return EmptyList;
            }
        }

        public static void SaveList()
        {
            String temp = XMLManager.SerializeObject<List<EventItem>>(FoodCollection.FoodList);
            Details.SetString(temp, "foodList");
        }

        public static String GetDate()
        {
            String temp = Details.StringForKey("date");
            if (temp == null)
            {
                return DateTime.Today.ToString();
            }
            else
            {
                return temp;
            }
        }

        public static void SaveDate()
        {
            Details.SetString(DateTime.Today.ToString(), "date");
            Console.WriteLine(DateTime.Today.ToString());
        }

        public static Boolean firstLaunch()
        {
            Boolean check = Details.BoolForKey("firstTime");
            Console.WriteLine(check);
            return check;
        }

        public static void SaveAll()
        {
            SaveCount();
            SaveList();
            SaveDate();
        }
    }
}
