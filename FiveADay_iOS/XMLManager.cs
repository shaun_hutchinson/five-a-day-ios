using System.Collections.Generic;
using System.Xml.Serialization;
using System.IO;

namespace FiveADay_iOS
{
    public static class XMLManager
    {
        private static List<EventItem> temp;

        public static string SerializeObject<T>(this T toSerialize)
        {
            XmlSerializer xml = new XmlSerializer(typeof(List<EventItem>));

            using (StringWriter textWriter = new StringWriter())
            {
                xml.Serialize(textWriter, toSerialize);
                return textWriter.ToString();
            }
        }

        public static List<EventItem> DeserializeObject(string result)
        {
            
            XmlSerializer xml = new XmlSerializer(typeof(List<EventItem>));

            using (TextReader reader = new StringReader(result))
            {
                temp = (List<EventItem>)xml.Deserialize(reader);
            }

            return temp;
        }
    }
}