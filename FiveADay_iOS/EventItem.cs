﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FiveADay_iOS
{
    public class EventItem
    {
        public String Name;
        public String Date;
        public Boolean IsFruit;


        public EventItem(){}

        public EventItem(String name, String date, Boolean isFruit)
        {
            this.Name = name;
            this.Date = date;
            this.IsFruit = isFruit;
        }


    }
}
