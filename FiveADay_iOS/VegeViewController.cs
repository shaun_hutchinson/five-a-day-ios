using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace FiveADay_iOS
{
    partial class VegeViewController : UICollectionViewController
    {
        static NSString vegeCellId = new NSString("VegeCell");
        static NSString headerId = new NSString("Header");

        static String[] imageNames = { "asparagus.png", "beans.png", "broccoli.png", "cabbage.png", "carrot.png", "cauliflower.png", "corn.png", "kumara.png", "lettuce.png", "mushroom.png", "onion.png", "peas.png", "potato.png", "pumpkin.png", "yams.png" };
        private String Date = DateTime.Now.ToShortTimeString();

        public VegeViewController(IntPtr handle) : base(handle)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            CollectionView.RegisterClassForCell(typeof(VegeCell), vegeCellId);

        }

        public override nint NumberOfSections(UICollectionView collectionView)
        {
            return 1;
        }

        public override nint GetItemsCount(UICollectionView collectionView, nint section)
        {
            return imageNames.Length;
        }

        //the item click to add vege to eaten items
        public override void ItemHighlighted(UICollectionView collectionView, NSIndexPath indexPath)
        {
            var cell = collectionView.CellForItem(indexPath);
            String Name = (imageNames[indexPath.Row].Substring(0, imageNames[indexPath.Row].Length - 4));
            EventItem item = new EventItem(Name, Date, false);
            FoodCollection.FoodList.Add(item);

            Count.Decrement();
            Console.WriteLine(Count.GetCount() + ", I should of decremented here");
            this.NavigationController.PopViewController(true);

        }

        public override UICollectionViewCell GetCell(UICollectionView collectionView, NSIndexPath indexPath)
        {
            var vegeCell = (VegeCell)collectionView.DequeueReusableCell(vegeCellId, indexPath);
            vegeCell.Image = UIImage.FromFile("Images/" + imageNames[indexPath.Row]);
            return vegeCell;
        }

    }
}
